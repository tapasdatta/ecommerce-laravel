<?php

namespace ecommerce;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public $rules = [
    	'name' => 'required'
    	];

    public function products()
    {
    	return $this->hasMany(Product::class);
    }
}
