<?php

namespace ecommerce\Providers;

use Illuminate\Support\ServiceProvider;

use ecommerce\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      if(!is_null(Category::all())) {
        view()->share('catnav', Category::all());
      }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
