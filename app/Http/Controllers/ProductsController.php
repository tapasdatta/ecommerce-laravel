<?php

namespace ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use ecommerce\Http\Requests;
use ecommerce\Product;
use ecommerce\Category;
use Intervention\Image\ImageManagerStatic as Image;

class ProductsController extends Controller
{
    /**
     * Restrict the controllers for other users
     * use 'admin' middleware
    */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Pass a all products as a variable to the 'index' view
     * using the compact method as
     * the second parameter.
     * specify the collection to be keyed
     * using 'pluck method'. ex:
     * ['prod-100' => 'Desk', 'prod-200' => 'Chair']
    */
    public function index()
    {
    	$products = Product::all();

    	return view('products.index', compact('products'))
          ->with('categories', Category::pluck('name', 'id'));
    }

    /**
     * Save new product,
     * typehint the category object to get category id
     * using Image facade
    */
    public function store(Request $request, Category $category)
    {
    	$this->validate($request, [
    		'description' => 'required|min:3',
    		]);
      //return $request->all();
      $product = new Product;
  		$product->category_id = $request->category_id;
  		$product->title = $request->title;
  		$product->description = $request->description;
  		$product->price = $request->price;
  		$image = $request->file('image');
  		$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
  		Image::configure(array('driver' => 'imagick'));
  		$path = public_path('assets/img/products/'.$filename);
  		Image::make($image->getRealPath())->resize(468, 249)->save($path);
  		$product->image = 'assets/img/products/'.$filename;
  		$product->save();

      return back();
    }

    /**
     * Update the product
     * typehint product object
    */
    public function update(Request $request, Product $products)
    {

    	$products->where('id', $request->id)
        ->update([
          'availability' => $request->availability
        ]);

    	return back();
    }

    /**
     * Delete the product
     * Type-hint the product object in 'destroy' method
    */
    public function destroy(Product $products)
    {
    	$products->delete();

    	return back();
    }
}
