<?php

namespace ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use ecommerce\Http\Requests;
use ecommerce\Product;
use ecommerce\Category;
use Cart;

class StoreController extends Controller
{

    /**
     * Restrict the controllers for un-authenticated users
     * apply only for 'addToCart' and 'getCart' method
    */
    public function __construct()
    {
        $this->middleware('auth', [
        'only' => [
            'addToCart',
            'getCart'
          ]
        ]);
    }

    /**
     * Display the products
     * Passes the second parameter to get 4 products
    */
    public function show()
    {
    	return view('store.index')
      ->with('products', Product::take(4)
      ->get());
    }

    /**
     * Pass a all products as a variable to the store 'view' view
     * using the compact method as
     * the second parameter.
    */
    public function view(Product $product)
    {
    	return view('store.view', compact('product'));
    }

    /**
     * Pass a all products as a variable to the 'category' view
     * paginate
     * passes category variable using with method
    */
    public function category(Category $category)
    {
    	return view('store.category')
    	->with('products', Product::where('category_id', '=', $category->id)
      ->paginate(3))
    	->with('category', $category);
    }

    /**
     * Return the search results to the 'search' view
    */
    public function search(Request $request)
    {
        $keyword = $request->keyword;

        return view('store.search')
        ->with('products', Product::where('title', 'LIKE', '%'.$keyword.'%')
        ->get())
        ->with('keyword', $keyword);
    }

    /**
     * Add the item to the cart
     * using the moltin/cart facade
    */
    public function addToCart(Request $request)
    {
        $product = Product::find($request->id);

        Cart::insert(array(
            'id'        =>  $product->id,
            'name'      =>  $product->title,
            'price'     =>  $product->price,
            'quantity'  =>  $request->quantity,
            'image'     =>  $product->image
        ));

        return redirect('store/cart');
    }

    /**
     * Get all data/items stored in the Cart object
    */
    public function getCart()
    {
        return view('store.cart')
        ->with('products', Cart::contents());
    }

    /**
     * Remove item from cart
     * redirect the user to the 'cart' named route
    */
    public function removeItem($identifier)
    {
        $item = Cart::item($identifier);
        $item->remove();

        return redirect('cart');
    }
}
