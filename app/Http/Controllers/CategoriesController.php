<?php

namespace ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use ecommerce\Http\Requests;
use ecommerce\Category;

class CategoriesController extends Controller
{
    /**
     * Restrict the controllers for other users
     * use 'admin' middleware
    */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Pass a all categories as a variable to the 'index' view
     * using the compact method as
     * the second parameter.
    */
    public function index()
    {
    	$categories = Category::all();

    	return view('categories.index', compact('categories'));
    }

    /**
     * Save new category
     * validate form data
    */
    public function store(Request $request, Category $categories)
    {
    	$this->validate($request, [
    		'name' => 'required|min:3'
    		]);

    	$categories->name = $request->name;

    	$categories->save();

    	return back();
    }

    /**
     * Delete the category
     * Type-hint the Category object in 'destroy' method
    */
    public function destroy(Category $categories)
    {
      $categories->delete();

      return back();
    }
}
