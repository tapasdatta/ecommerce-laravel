<?php

namespace ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use ecommerce\Http\Requests;
use ecommerce\User;
use Validator;
use Hash;
use Auth;

class UsersController extends Controller
{


  /**
   * Load the newaccount view
  */
  public function newaccount() {

		return view('users.newaccount');

	}

  /**
   * Sign up new user
   * passes the request object data from form
   * passed the rules from User model
   * valdate the data using Validator
  */
	public function create(Request $request)
	{
		$validator = Validator::make($request->all(), User::$rules);

		if($validator->passes()) {
			$user = new User;
			$user->firstname = $request->firstname;
			$user->lastname = $request->lastname;
			$user->email = $request->email;
			$user->password = Hash::make($request->password);
			$user->telephone = $request->telephone;
			$user->save();

			return redirect('users/signin');
		}

		return redirect('users/newaccount')
			->with('message', 'Something went wrong')
			->withErrors($validator)
			->withInput();
	}

  /**
   * Load the 'signin' view
  */
	public function signin()
	{
		return view('users.signin');
	}

  /**
   * Login the user
   * using the Auth
  */
  public function postSignin(Request $request)
	{
		if (Auth::attempt([
        'email'=>$request->email,
        'password'=>$request->password
        ])) {
			return redirect('/');
		}

		return redirect('users/signin')
    ->with('message', 'Your email/password combo was incorrect');
	}

  /**
   * logout the user
   * redirect to the signin route
  */
	public function signout() {
		Auth::logout();
		return redirect('users/signin');
	}

}
