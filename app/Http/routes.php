<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//(ps: cached the all routes)

Route::group(['middleware' => ['web']], function() {

  Route::get('/', 'StoreController@show');

  //admin only pages/routes
  Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {;
    Route::resource('categories', 'CategoriesController');
    Route::resource('products', 'ProductsController');
  });

  //store pages/routes
  Route::group(['prefix' => 'store'], function() {
    Route::get('view/{product}', 'StoreController@view');
    Route::get('category/{category}', 'StoreController@category');
    Route::get('search', 'StoreController@search');
    Route::post('addtocart', 'StoreController@addToCart');
    Route::get('cart', ['as' => 'cart', 'uses' => 'StoreController@getCart']);
    Route::get('removeitem/{identifier}', 'StoreController@removeItem');
  });

  //user pages/routes
  Route::group(['prefix' => 'users'], function() {
    Route::get('newaccount', 'UsersController@newaccount');
    Route::post('create', 'UsersController@create');
    Route::get('signin', 'UsersController@signin');
    Route::post('signin', 'UsersController@postSignin');
    Route::get('signout', 'UsersController@signout');
  });

});
