<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>eCommerce</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        {{ Html::style('assets/css/normalize.css') }}
        {{ Html::style('assets/css/main.css') }}
        {{ Html::script('assets/js/vendor/modernizr-2.6.2.min.js') }}
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <div id="wrapper">
            <header>
                <section id="top-area">
                    <p>Phone orders: 1-800-0000 | Email us: <a href="mailto:tapas@shop.com">office@shop.com</a></p>
                </section><!-- end top-area -->
                <section id="action-bar">
                    <div id="logo">
                        <a href="/"><span id="logo-accent">e</span>Commerce</a>
                    </div><!-- end logo -->

                    <nav class="dropdown">
                        <ul>
                            <li>
                                <a href="#">Shop by Category {{ Html::image('assets/img/down-arrow.gif', 'Sign in') }}</a>
                                <ul>
                                    @foreach($catnav as $cat)
                                        <li>{{ HTML::link('/store/category/'.$cat->id, $cat->name) }}</li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </nav>

                    <div id="search-form">
                        {{ Form::open(array('url'=>'store/search', 'method'=>'get')) }}
                        {{ Form::text('keyword', null, array('placeholder'=>'Search by keyword', 'class'=>'search')) }}
                        {{ Form::submit('Search', array('class'=>'search submit')) }}
                        {{ Form::close() }}
                    </div><!-- end search-form -->

                    <div id="user-menu">
                        @if(Auth::check())
                            <nav class="dropdown">
                                <ul>
                                    <li>
                                        <a href="#">{{ HTML::image('assets/img/user-icon.gif', Auth::user()->firstname) }} {{ Auth::user()->firstname }} {{ HTML::image('assets/img/down-arrow.gif', Auth::user()->firstname) }}</a>
                                        <ul>
                                            @if(Auth::user()->isAdmin())
                                                <li>{{ HTML::link('admin/categories', 'Manage Categories') }}</li>
                                                <li>{{ HTML::link('admin/products', 'Manage Products') }}</li>
                                            @endif
                                            <li>{{ HTML::link('users/signout', 'Sign Out') }}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        @else
                            <nav id="signin" class="dropdown">
                                <ul>
                                    <li>
                                        <a href="#">{{ HTML::image('assets/img/user-icon.gif', 'Sign In') }} Sign In {{ HTML::image('assets/img/down-arrow.gif', 'Sign In') }}</a>
                                        <ul>
                                            <li>{{ HTML::link('users/signin', 'Sign In') }}</li>
                                            <li>{{ HTML::link('users/newaccount', 'Sign Up') }}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        @endif
                    </div><!-- end user-menu -->

                    <div id="view-cart">
                        <a href="{{ route('cart') }}">{{ Html::image('/assets/img/blue-cart.gif')}} View Cart</a>
                    </div><!-- end view-cart -->
                </section><!-- end action-bar -->
            </header>

            <hr />
            @yield('promo')

            @yield('search-keyword')

            <section id="main-content" class="clearfix">
                @if(Session::has('message'))
                    <p class="alert">{{ Session::get('message') }}</p>
                @endif

                @yield('content')
            </section><!-- end main-content -->

            <hr />
            @yield('pagination')

            <footer>
                <section id="contact">
                    <h3>For phone orders please call 1-800-000. You<br>can also email us at <a href="mailto:office@shop.com">office@shop.com</a></h3>
                </section><!-- end contact -->

                <hr />

                <section id="links">
                    <div id="my-account">
                        <h4>MY ACCOUNT</h4>
                        <ul>
                            <li>{{ HTML::link('users/signin', 'Sign In') }}</li>
                            <li>{{ HTML::link('users/newaccount', 'Sign Up') }}</li>
                            <li><a href="#">Order History</a></li>
                            <li>{{ HTML::link('store/cart', 'Cart') }}</li>
                        </ul>
                    </div><!-- end my-account -->
                    <div id="info">
                        <h4>INFORMATION</h4>
                        <ul>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div><!-- end info -->
                    <div id="extras">
                        <h4>EXTRAS</h4>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div><!-- end extras -->
                </section><!-- end links -->

                <hr />

                <section class="clearfix">
                    <div id="copyright">
                        <div id="logo">
                            <a href="#"><span id="logo-accent">e</span>Commerce</a>
                        </div><!-- end logo -->
                        <p id="store-desc">This is a short description of the store.</p>
                        <p id="store-copy">&copy; 2013 eCommerce. Custom theme from PSD.</p>
                    </div><!-- end copyright -->
                    <div id="connect">
                        <h4>CONNECT WITH US</h4>
                        <ul>
                            <li class="twitter"><a href="#">Twitter</a></li>
                            <li class="fb"><a href="#">Facebook</a></li>
                        </ul>
                    </div><!-- end connect -->
                    <div id="payments">
                        <h4>SUPPORTED PAYMENT METHODS</h4>
                        {{ Html::image("assets/img/payment-methods.gif")}}
                    </div><!-- end payments -->
                </section>
            </footer>
        </div><!-- end wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
