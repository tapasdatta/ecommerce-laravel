@extends('layout.main')

@section('content')

<div id="admin">

	<h1>Products Admin Panel</h1><hr>

	<p>Here you can view, delete, and create new products.</p>

	<h2>Products</h2><hr>

	<ul>
		@foreach($products as $product)
			<li>
				{{ Html::image($product->image, $product->title, array('width' => 50))}}
				{{ $product->title }}
				{{ Form::open(array('method' => 'POST', 'action' => array('ProductsController@destroy', $product->id),  'class'=>'form-inline'))}}
				{{ method_field('DELETE') }}
				{{ Form::submit('Delete') }}
				{{ Form::close() }}

				{{ Form::open(array('method' => 'POST', 'url'=>"admin/products/{$product->id}", 'class'=>'form-inline'))}}
				{{ method_field('PUT') }}
				{{ Form::hidden('id', $product->id) }}
				{{ Form::select('availability', array('1'=>'In Stock', '0'=>'Out of Stock'), $product->availability) }}
				{{ Form::submit('Update') }}
				{{ Form::close() }}
			</li>
		@endforeach
	</ul>
	<h2>Create New Product</h2><hr>

		@if(count($errors))
		<div id="form-errors">
			<p>The following errors have occurred:</p>

			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><!-- end form-errors -->
		@endif

		{{ Form::open(array('url'=>'admin/products', 'files'=>true)) }}
		<p>
			{{ Form::label('category_id', 'Category') }}
			{{ Form::select('category_id', $categories) }}
		</p>
		<p>
			{{ Form::label('title') }}
			{{ Form::text('title') }}
		</p>
		<p>
			{{ Form::label('description') }}
			{{ Form::textarea('description') }}
		</p>
		<p>
			{{ Form::label('price') }}
			{{ Form::text('price', null, array('class'=>'form-price')) }}
		</p>
		<p>
			{{ Form::label('image', 'Choose an image') }}
			{{ Form::file('image') }}
		</p>
		{{ Form::submit('Create Product', array('class'=>'secondary-cart-btn')) }}
		{{ Form::close() }}
</div>

@stop
