@extends('layout.main')

@section('content')
	<div id="admin">
		<h1>Admin Categories</h1><hr>
		<p>Here you can add category</p>

		<h2>Create Category</h2><hr>
		<ul>
		@foreach($categories as $category)
			<li>
				{{ $category->name }}
				{{ Form::open(array('method' => 'POST', 'action' => array('CategoriesController@destroy', $category->id),  'class'=>'form-inline'))}}
				{{ method_field('DELETE') }}
				{{ Form::submit('Delete') }}
				{{ Form::close() }}
			</li>
		@endforeach
		</ul>

		@if(count($errors->all()))
			<div class="alert alert-danger" role="alert">
				<strong>Whoops! Something went wrong!</strong
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<form method="POST" action="/admin/categories">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-controll" name="name">
			</div>
			<button class="btn btn-default">Add</button>
		</form>
	</div>
@stop
